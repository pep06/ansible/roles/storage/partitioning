# Ansible role ``storage/partitioning``

## Description
This role is a simple wrapper around more specific sub-roles. It is used
to delegate the configuration of storage devices to a specialized
role fitting the platform of the configured host.

## Providers
This role delegates network configuration to one of the following
provider roles, according to the value of the ``ansible_distribution``
variable on the configured host:

| ``ansible_distribution`` | Provider role                   |
| ------------------------ | ------------------------------- |
| ``CentOS``               | ``storage/partitioning.parted`` |
| ``Debian``               | ``storage/partitioning.parted`` |